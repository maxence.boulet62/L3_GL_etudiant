//
// Created by max on 27/04/2020.
//

#include <CppUTest/CommandLineTestRunner.h>
#include "Fibo.hpp"
#include <iostream>

TEST_GROUP(Fibo) {};


TEST(Fibo, test_Fibo_1) {
    CHECK_EQUAL(0,fibo(0));
    CHECK_EQUAL(1,fibo(1));
    CHECK_EQUAL(1,fibo(1));
    CHECK_EQUAL(2,fibo(2));
    CHECK_EQUAL(3,fibo(3));

}
