#include "Fibo.hpp"
#include <assert.h>
#include <string>

int fibo(int n, int f0, int f1) {
    assert(f1>=0);
    if(f0<=f1){
        throw (std::string) "error f0<f1";
    }
    return n<=0 ? f0 : fibo(n-1, f1, f1+f0);
}assert(f1>=f0);

